package main

import (
	"fmt"
	"net"
	"os"
)

func listIPs(subnets []string) ([]string, error) {
	var ips []string
	for _, subnet := range subnets {
		_, ipNet, err := net.ParseCIDR(subnet)
		if err != nil {
			return nil, fmt.Errorf("Invalid subnet: %s", subnet)
		}
		for ip := ipNet.IP.Mask(ipNet.Mask); ipNet.Contains(ip); increment(ip) {
			ips = append(ips, ip.String())
		}
	}
	return ips, nil
}

func increment(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: iplist <subnet1> <subnet2> ...")
		return
	}

	subnets := os.Args[1:]
	ips, err := listIPs(subnets)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, ip := range ips {
		fmt.Println(ip)
	}
}
