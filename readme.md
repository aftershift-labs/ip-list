# Gitlab CI Release Automation

Gitlab CI üzerinde çoklu platform destekli bir GO projesi için release otomasyonu oluşturmak

&nbsp;

## IP List (Örnek Proje)

Öncelikle GO ile geliştirilmiş bir proje oluşturalım.

Uygulamamız, parametre olarak verilen subnetlerin IP adreslerini listeleyecek.

```go
// file: main.go

package main

import (
	"fmt"
	"net"
	"os"
)

func listIPs(subnets []string) ([]string, error) {
	var ips []string
	for _, subnet := range subnets {
		_, ipNet, err := net.ParseCIDR(subnet)
		if err != nil {
			return nil, fmt.Errorf("Invalid subnet: %s", subnet)
		}
		for ip := ipNet.IP.Mask(ipNet.Mask); ipNet.Contains(ip); increment(ip) {
			ips = append(ips, ip.String())
		}
	}
	return ips, nil
}

func increment(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: iplist <subnet1> <subnet2> ...")
		return
	}

	subnets := os.Args[1:]
	ips, err := listIPs(subnets)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, ip := range ips {
		fmt.Println(ip)
	}
}
```

&nbsp;

Örnek kullanım:

```sh
go run main.go 192.0.2.0/30 172.16.31.10/30
```
```
# output:

192.0.2.0
192.0.2.1
192.0.2.2
192.0.2.3
172.16.31.8
172.16.31.9
172.16.31.10
172.16.31.11
```

&nbsp;

&nbsp;

### Elle Derleme Adımları

Evvela işlemleri elle yürütebiliyor olmalıyız ki otomatize edebilelim.

Derleme komutlarımız bu şekilde olacak:

```sh
go mod init ip-list

go mod tidy

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
```

&nbsp;

```sh
go mod init ip-list
```
```
# output:

go: creating new go.mod: module ip-list
go: to add module requirements and sums:
        go mod tidy
```
```sh
go mod tidy
```
```sh
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
```
```sh
./ip-list 192.0.2.0/30 172.16.31.10/30
```
```
# output:

192.0.2.0
192.0.2.1
192.0.2.2
192.0.2.3
172.16.31.8
172.16.31.9
172.16.31.10
172.16.31.11
```
```sh
tree
```
```
# output:

.
├── go.mod
├── ip-list
├── main.go
└── README.md

1 directory, 4 files
```

&nbsp;

Gereksiz dosyaları görmezden gelmek için .gitignore dosyası oluşturuyoruz.

Bu sayede çalışma ortamımızı temiz tutacağız.

```
# file: .gitignore

go.mod
go.sum
ip-list
```

&nbsp;

&nbsp;

## Gitlab CI

&nbsp;

### Build

İlk adım olarak *.gitlab-ci.yml* dosyasını oluşturup, bu dosya içerisinde *build* işlemini tanımlıyoruz.

Bu projedeki tüm işlemler için paylaşımlı Runner'ları kullanabiliriz. Bu yüzden etiket tanımlamamıza gerek yok.

```yaml
# file: .gitlab-ci.yml

Build:
  stage: build
  image: golang:1.22.4-alpine3.20
  before_script:
    - go mod init ip-list
    - go mod tidy
  script:
    - CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
```

&nbsp;

Bu şekilde kodumuzu Gitlab'a push'layıp, pipeline'ı çalıştırarak sonucu görebiliriz.

&nbsp;

<div align="center">
    <img src=".img/1.png">
</div>

&nbsp;

Projemizin derlendiğini gördük. Fakat herhangi bir artifact elde edemedik. Buna sonraki adımlarda bakacağız.

&nbsp;

&nbsp;

### Test

GO ile çok basit bir şekilde birim testleri yazabiliriz. Bunu Gitlab'a entegre etmek de muhtemelen sandığınızdan çok daha basit olacak :)

Öncelikle GO projemiz için unit test'ler oluşturalım.

Aşağıdaki kod bloğunda 3 adet test senaryosu bulunuyor:

- İlk test senaryosunda, 1 adet subnet girilerek, beklenen 4 adet IP adresini döndürdüğü doğrulanıyor.
- İkinci test senaryosunda, 2 adet subnet girilerek, beklenen 8 adet IP adresini döndürdüğü doğrulanıyor.
- Üçüncü test senaryosunda, geçersiz bir subnet girilerek, hata döndürdüğü doğrulanıyor.

```go
// file: main_test.go

package main

import (
	"reflect"
	"testing"
)

func TestListIPs(t *testing.T) {
	tests := []struct {
		name     string
		subnets  []string
		expected []string
	}{
		{
			name:    "Single subnet",
			subnets: []string{"192.168.1.0/30"},
			expected: []string{
				"192.168.1.0",
				"192.168.1.1",
				"192.168.1.2",
				"192.168.1.3",
			},
		},
		{
			name:    "Multiple subnets",
			subnets: []string{"192.168.1.0/30", "10.0.0.0/30"},
			expected: []string{
				"192.168.1.0",
				"192.168.1.1",
				"192.168.1.2",
				"192.168.1.3",
				"10.0.0.0",
				"10.0.0.1",
				"10.0.0.2",
				"10.0.0.3",
			},
		},
		{
			name:    "Invalid subnet",
			subnets: []string{"invalid_subnet"},
			expected: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual, err := listIPs(tt.subnets)
			if tt.expected == nil && err == nil {
				t.Errorf("expected error but got nil")
			}
			if tt.expected != nil && err != nil {
				t.Errorf("did not expect error but got %v", err)
			}
			if !reflect.DeepEqual(actual, tt.expected) {
				t.Errorf("expected %v, got %v", tt.expected, actual)
			}
		})
	}
}

```

&nbsp;

Şimdi testleri yine elle koşturalım.

```sh
go test -v
```
```
# output:

=== RUN   TestListIPs
=== RUN   TestListIPs/Single_subnet
=== RUN   TestListIPs/Multiple_subnets
=== RUN   TestListIPs/Invalid_subnet
--- PASS: TestListIPs (0.00s)
    --- PASS: TestListIPs/Single_subnet (0.00s)
    --- PASS: TestListIPs/Multiple_subnets (0.00s)
    --- PASS: TestListIPs/Invalid_subnet (0.00s)
PASS
ok      ip-list 0.005s
```

&nbsp;

Uygulamamızın beklendiği şekilde çalıştığını doğrulamış olduk.

&nbsp;

Gitlab CI, varsayılan olarak *xml* formatındaki *Junit test rapor*larını destekler.

Bu adımı piplene'a eklemeden önce yine elle test edelim.

Rapor oluşturmak için *github.com/jstemmer/go-junit-report/v2@latest* paketini kullanacağız.

```sh
go install github.com/jstemmer/go-junit-report/v2@latest
```
```
# output:

go: downloading github.com/jstemmer/go-junit-report/v2 v2.1.0
go: downloading github.com/jstemmer/go-junit-report v1.0.0
```
```sh
go test -v 2>&1 | go-junit-report > report.xml

cat report.xml
```
```
# output:

<?xml version="1.0" encoding="UTF-8"?>
<testsuites tests="4">
        <testsuite name="ip-list" tests="4" failures="0" errors="0" id="0" hostname="oxw" time="0.006" timestamp="2024-07-03T23:27:08+03:00">
                <testcase name="TestListIPs" classname="ip-list" time="0.000"></testcase>
                <testcase name="TestListIPs/Single_subnet" classname="ip-list" time="0.000"></testcase>
                <testcase name="TestListIPs/Multiple_subnets" classname="ip-list" time="0.000"></testcase>
                <testcase name="TestListIPs/Invalid_subnet" classname="ip-list" time="0.000"></testcase>
        </testsuite>
</testsuites>
```

&nbsp;

Şimdi piplene'a ekleyerek testlerin her commit'te çalışmasını sağlayabiliriz.

Aşağıdaki bloğu biraz açıklayayım:

- *Unit Tests* işi altında:

  - *before_script* bloğu altında GO projemizin test edilebilmesi için gerekli hazırlıklar yapılır.

  - *script* bloğunda da testler çalıştırılır.

  - *artifacts* bloğu altında ise test sonuçlarının Gitlab'a gönderilmesini sağlayan *junit report* kullanılır.

    - Koşturulan her testin sonucu 1 hafta süre ile artifact olarak saklanır ve pipeline'ımızın *Tests* sekmesinde görülebilir.

```yaml
# .gitlab-ci.yml

Unit Tests:
  stage: test
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: always
    paths:
      - report.xml
    expire_in: 1 week
    reports:
      junit: report.xml
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - go install github.com/jstemmer/go-junit-report/v2@latest
  script:
    - go test -v 2>&1 | go-junit-report > report.xml

Build:
  stage: build
  image: golang:1.22.4-alpine3.20
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
  script:
    - CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
```

&nbsp;

<div align="center">
    <img src=".img/2.png">
</div>

&nbsp;

Pipeline'ımız çalıştı fakat bir terslik var gibi görünüyor.

Test adımı, Build adımından sonra çalıştı. Bu da tercih edilebilir ama bizim sürecimize uygun değil.

Bu yüzden stage'leri elle belirlemeliyiz.

```yaml
# .gitlab-ci.yml

stages:
  - Test
  - Build

Unit Tests:
  stage: Test
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: always
    paths:
      - report.xml
    expire_in: 1 week
    reports:
      junit: report.xml
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - go install github.com/jstemmer/go-junit-report/v2@latest
  script:
    - go test -v 2>&1 | go-junit-report > report.xml

Build:
  stage: Build
  image: golang:1.22.4-alpine3.20
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
  script:
    - CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
```

&nbsp;

Test ve Build stage'lerini istediğimiz gibi sıralayarak baş harflerini büyük yazdık.

Gitlab CI'da bazı alanları şık görünecek şekilde yazabiliriz. Büyük/küçük harf, boşluk ve / gibi karakterler de kullanılabilir.

&nbsp;

<div align="center">
    <img src=".img/3.png">
</div>

&nbsp;

Test stage'i tamamlandığında sol tarafta görünen Test sekmesinin yanına **4** ibaresinin eklendiğini görüyoruz.

&nbsp;

<div align="center">
    <img src=".img/4.png">
</div>

&nbsp;

Unit Tests detayında, yürütülen testlerin sonuçlarını görebilirsiniz.

&nbsp;

<div align="center">
    <img src=".img/5.png">
</div>

&nbsp;

&nbsp;

### Artifacts

Şimdi derleme ve test adımlarını tamamlamış olduk. Fakat hala derlenmiş dosyalara ulaşamıyoruz.

Bunun için artifacts'i kullanabiliriz.

```yaml
# .gitlab-ci.yml

stages:
  - Test
  - Build

Unit Tests:
  stage: Test
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: always
    paths:
      - report.xml
    expire_in: 1 week
    reports:
      junit: report.xml
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - go install github.com/jstemmer/go-junit-report/v2@latest
  script:
    - go test -v 2>&1 | go-junit-report > report.xml

Build:
  stage: Build
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: on_success
    paths:
      - ip-list*
    expire_in: 1 week
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
  script:
    - CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
```

&nbsp;

Build stage'i tamamlandığında sağ tarafta *Job artifacts* alanında indirilebilir bir içerik olduğunu görüyoruz.

&nbsp;

<div align="center">
    <img src=".img/6.png">
</div>

&nbsp;

<div align="center">
    <img src=".img/7.png">
</div>

&nbsp;

Derlenmiş binary'i elde ettiğimize göre pipeline'ımızı biraz geliştirelim.

Öncelikle *go build* komutundan önce eklediğimiz değişkenleri Gitlab CI'a ekleyelim.

Bu, tek bir işletim sistemi ve mimari için uygun bir yöntemdir. 

```yaml
# .gitlab-ci.yml

stages:
  - Test
  - Build

Unit Tests:
  stage: Test
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: always
    paths:
      - report.xml
    expire_in: 1 week
    reports:
      junit: report.xml
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - go install github.com/jstemmer/go-junit-report/v2@latest
  script:
    - go test -v 2>&1 | go-junit-report > report.xml

Build:
  stage: Build
  image: golang:1.22.4-alpine3.20
  variables:
    CGO_ENABLED: 0
    GOOS: linux
    GOARCH: amd64
  artifacts:
    when: on_success
    paths:
      - ip-list*
    expire_in: 1 week
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
  script:
    - go build
```

&nbsp;

Fakat ben hem GNU/Linux, MacOS/Darwin ve Windows işletim sistemlerinde çalışabilecek hem de AMD64 ve ARM64 mimarilerini destekleyen ayrı ayrı çalıştırılabilir ikilik dosyalar oluşturmak istiyorum.

Bunun için en güzel yöntem Gitlab CI'ın *parallel.matrix* özelliğini kullanmak olacak.

Bu sayede hem ayrı ayrı job'lar oluşturmak zorunda kalmayacağım hem de işlemleri paralel olarak yürütebileceğim.

```yaml
# .gitlab-ci.yml

stages:
  - Test
  - Build

Unit Tests:
  stage: Test
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: always
    paths:
      - report.xml
    expire_in: 1 week
    reports:
      junit: report.xml
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - go install github.com/jstemmer/go-junit-report/v2@latest
  script:
    - go test -v 2>&1 | go-junit-report > report.xml

Build:
  stage: Build
  image: golang:1.22.4-alpine3.20
  variables:
    CGO_ENABLED: 0
  parallel:
    matrix:
      - GOARCH:
          - arm64
          - amd64
        GOOS:
          - linux
          - darwin
          - windows
  artifacts:
    when: on_success
    paths:
      - ip-list*
    expire_in: 1 week
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
  script:
    - go build
```

&nbsp;

Yukarıdaki blokta *variables* altında sadece *CGO_ENABLED* değişkenini bıraktım. Bu hepsinde geçerli olacak.

Buna ilaveten *parallel.matrix* bloğunu ekledim (parallel'in matrix olmadan kendi başına ayrı bir kullanımı var. Buna değinmeyeceğim.)

Kısaca özetlemek gerekirse *parallel.matrix*, altında tanımlanan dizileri bir biri ile çaprazlayarak bir matrix oluşturur.

Yukarıdaki yapıda **6** adet job oluşacak:

- arm64/linux
- arm64/darwin
- arm64/windows
- amd64/linux
- amd64/darwin
- amd64/windows

Aynı zamanda her job için *GOARCH* ve *GOOS* değişkenleri ayrı ayrı tanımlanmış olacak.

&nbsp;

Bu şekilde devam edersek aynı isimde birden fazla artifact elde edeceğiz. Bunların karışmasını istemeyiz. Bu sebeple her mimari ve platform için ayrı dizinler oluşmasını sağlayalım.

*binaries/\$GOOS/\$GOARCH* dizinini oluşturup ikilik dosyanın bu dizin altına taşınmasını sağlıyorum.

Buna ilaveten artifacts.paths altına *binaries/\$GOOS/\$GOARCH* dizini ekledim.

```yaml
# .gitlab-ci.yml

stages:
  - Test
  - Build

Unit Tests:
  stage: Test
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: always
    paths:
      - report.xml
    expire_in: 1 week
    reports:
      junit: report.xml
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - go install github.com/jstemmer/go-junit-report/v2@latest
  script:
    - go test -v 2>&1 | go-junit-report > report.xml

Build:
  stage: Build
  image: golang:1.22.4-alpine3.20
  variables:
    CGO_ENABLED: 0
  parallel:
    matrix:
      - GOARCH:
          - arm64
          - amd64
        GOOS:
          - linux
          - darwin
          - windows
  artifacts:
    when: on_success
    paths:
      - binaries/$GOOS/$GOARCH
    expire_in: 1 week
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - mkdir -p binaries/$GOOS/$GOARCH
  script:
    - go build
    - mv ip-list* binaries/$GOOS/$GOARCH/
```
&nbsp;

<div align="center">
    <img src=".img/8.png">
</div>

&nbsp;

Her bir job'ın detayında kendisinin derlemiş olduğu ikilik dosyayı görebilirsiniz.

&nbsp;

<div align="center">
    <img src=".img/9.png">
</div>

&nbsp;

Projenin *Build/Artifacts* sekmesi altına bakarsanız da tüm job'ların derlemiş olduğu ikili dosyaları görebilirsiniz.

&nbsp;

<div align="center">
    <img src=".img/10.png">
</div>

&nbsp;

Bu durumda pipeline'ım hala yeterince kullanışlı değil. Çünkü dosyaların tek bir yerde toplanmasını istiyorum.

Bu noktada bir çok yöntem ile devam edebiliriz.

- Tüm artifact'leri kendi üzerinde toplayacak bir job oluşturulabilir.
- Artifact'leri oluşturan job'ların, ikilik dosyaları Gitlab Package Registry'ye atması sağlanabilir.
- Artifact'leri oluşturan job'ların, ikilik dosyaları S3 gibi bir depoya atması sağlanabilir.

Ben ikinci yöntem ile devam edeceğim.

Gitlab; Docker imajları, Helm, NuGet gibi paketleri ve niteliksiz dosyaları depolayabileceğimiz Container ve Package Registry'ler sağlar.

Biz de ikilik dosyalarımızı saklamak için *Generic Package Registry* 'i kullanacağız.

```yaml
# .gitlab-ci.yml

stages:
  - Test
  - Build

Unit Tests:
  stage: Test
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: always
    paths:
      - report.xml
    expire_in: 1 week
    reports:
      junit: report.xml
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - go install github.com/jstemmer/go-junit-report/v2@latest
  script:
    - go test -v 2>&1 | go-junit-report > report.xml

Build:
  stage: Build
  image: golang:1.22.4-alpine3.20
  variables:
    CGO_ENABLED: 0
  parallel:
    matrix:
      - GOARCH:
          - arm64
          - amd64
        GOOS:
          - linux
          - darwin
          - windows
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - apk add --update --no-cache curl 1> /dev/null
  script:
    - go build
    - |-
      curl -sLf \
        -H "JOB-TOKEN: $CI_JOB_TOKEN" -T $(ls -1 ip-list*) \
        $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$CI_PROJECT_NAME/$CI_PIPELINE_IID/$GOOS/$GOARCH/$(ls -1 ip-list*)
```

&nbsp;

Yukarıdaki blokta:
  
  - *Build* job'ı için *artifacts* alanını kaldırdım. Bundan sonra ihtiyacımız olmayacak.

  - *golang:1.22.4-alpine3.20* imajı ile ayağa kaldırdığımız konteyner içine *curl* paketini kurdum.

  - Ve Gitlab Package Registry'ye dosyalarımızı API üzerinden gönderecek *curl* komutunu ekledim.

  - Her yeni pipeline çalıştığında paketlerimizin sürümlenmesini sağlamak için Gitlab'ın öntanımlı değişkenlerinden biri olan *CI_PIPELINE_IID* 'yi kullandım.

  - Registry'ye erişimde yetkilendirme için sadece ilgili pipeline'da geçerli olan ve yine Gitlab'ın öntanımlı değişkenlerden biri olan *CI_JOB_TOKEN* 'ı kullandım.

&nbsp;

Yine projemiz altında *Deploy/Package Registry* sekmesini açtığımda her platform ve mimari için ayrı ayrı oluşturulmuş ikilik dosyaları görebiliyorum.

&nbsp;

<div align="center">
    <img src=".img/11.png">
</div>

&nbsp;

&nbsp;

### Release

Artık projemizi otomatik bir şekilde test edip derleyebiliyoruz. Sıra geldi release oluşturmaya.

Bu kısımda yöntemi biraz daha değiştireceğiz. Öncelikle *semantic versiyonlama* ile devam etmek istiyorum. Burada *SDLC* yapısına göre farklı yöntemler de tercih edilebilir.

Standardımız şöyle olacak:

- Release çıkılacak her paket için yeni bir *tag* oluşturulacak.
  - Bu tag: release-0.1, release-0.1, release-1.3 şeklinde *release-* prefix'i ile başlayıp versiyon numaralarından oluşacak.
  - Pipeline IID (CI_PIPELINE_IID) build numarası olarak sona eklenecek.
    - Sonuç olarak her release bu şekilde isimlendirilecek: release-0.1.27, release-0.3.45, release-1.9.89

&nbsp;

Bunun için *Build* işini, belirli formata uygun *tag* oluşturulduğunda istediğimiz versiyonlama ile çalışacak şekilde şartlandırmalıyız.

```yaml
# .gitlab-ci.yml

stages:
  - Test
  - Build

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        VERSION: ${CI_COMMIT_TAG}.${CI_PIPELINE_IID}
    - if: $CI_COMMIT_BRANCH
      variables:
        VERSION: $CI_PIPELINE_IID

Unit Tests:
  stage: Test
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: always
    paths:
      - report.xml
    expire_in: 1 week
    reports:
      junit: report.xml
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - go install github.com/jstemmer/go-junit-report/v2@latest
  script:
    - go test -v 2>&1 | go-junit-report > report.xml

Build:
  stage: Build
  image: golang:1.22.4-alpine3.20
  variables:
    CGO_ENABLED: 0
  parallel:
    matrix:
      - GOARCH:
          - arm64
          - amd64
        GOOS:
          - linux
          - darwin
          - windows
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - apk add --update --no-cache curl 1> /dev/null
  script:
    - go build
    - |-
      curl -sLf \
        -H "JOB-TOKEN: $CI_JOB_TOKEN" -T $(ls -1 ip-list*) \
        $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$CI_PROJECT_NAME/$VERSION/$GOOS/$GOARCH/$(ls -1 ip-list*)
```

&nbsp;

Yukarıda *workflow* bloğunu ekledik. Bu sayede branch'ten ve tag'den tetiklenen pipeline'lar için iki farklı sürüm standardı oluşturmuş olduk.

&nbsp;

<div align="center">
    <img src=".img/12.png">
</div>

Hem branch üzerinden hem de tag oluşturarak pipeline'ı tetikledim ve Package Registry'ye tekrar geldiğimde **9** ve **0.1.10** sürümlerinin oluştuğunu gördüm.

Henüz *release-* prefix'i ile başlayan bir tag oluşturmadığım için bu ifadeyi görmüyorum.

&nbsp;

Şimdi release job'ını ekleyelim.

```yaml
# .gitlab-ci.yml

stages:
  - Test
  - Build
  - Release

variables:
  PACKAGE_PATH: $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$CI_PROJECT_NAME/$VERSION

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        VERSION: ${CI_COMMIT_TAG}.${CI_PIPELINE_IID}
    - if: $CI_COMMIT_BRANCH
      variables:
        VERSION: $CI_PIPELINE_IID

Unit Tests:
  stage: Test
  image: golang:1.22.4-alpine3.20
  artifacts:
    when: always
    paths:
      - report.xml
    expire_in: 1 week
    reports:
      junit: report.xml
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - go install github.com/jstemmer/go-junit-report/v2@latest
  script:
    - go test -v 2>&1 | go-junit-report > report.xml

Build / Push:
  stage: Build
  image: golang:1.22.4-alpine3.20
  variables:
    CGO_ENABLED: 0
  parallel:
    matrix:
      - GOARCH:
          - arm64
          - amd64
        GOOS:
          - linux
          - darwin
          - windows
  before_script:
    - go mod init $CI_PROJECT_NAME
    - go mod tidy
    - apk add --update --no-cache curl 1> /dev/null
  script:
    - go build
    - |-
      curl -sLf \
        -H "JOB-TOKEN: $CI_JOB_TOKEN" -T $(ls -1 ip-list*) \
        $PACKAGE_PATH/$GOOS/$GOARCH/$(ls -1 ip-list*)

Release:
  stage: Release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  variables:
    GIT_STRATEGY: none
  rules:
    - if: $CI_COMMIT_TAG =~ /^release-\d+\.\d+$/
  script:
    - echo Create release
  release:
    name: $VERSION
    description: $CI_PROJECT_NAME $VERSION
    tag_name: $CI_COMMIT_TAG
    assets:
      links:
      # WC
        - name: $CI_PROJECT_NAME $VERSION (Windows / ARM64 / executable binary)
          url:  $PACKAGE_PATH/windows/arm64/ip-list.exe
          link_type: package

        - name: $CI_PROJECT_NAME $VERSION (Windows / AMD64 / executable binary)
          url:  $PACKAGE_PATH/windows/amd64/ip-list.exe
          link_type: package

      # Darwin
        - name: $CI_PROJECT_NAME $VERSION (MacOS/Darwin / ARM64 / executable binary)
          url:  $PACKAGE_PATH/darwin/arm64/ip-list
          link_type: package

        - name: $CI_PROJECT_NAME $VERSION (MacOS/Darwin / AMD64 / executable binary)
          url:  $PACKAGE_PATH/darwin/amd64/ip-list
          link_type: package

      # Linux
        - name: $CI_PROJECT_NAME $VERSION (GNU/Linux / ARM64 / executable binary)
          url:  $PACKAGE_PATH/linux/arm64/ip-list
          link_type: package

        - name: $CI_PROJECT_NAME $VERSION (GNU/Linux / AMD64 / executable binary)
          url:  $PACKAGE_PATH/linux/amd64/ip-list
          link_type: package
```

&nbsp;

Release adında bir job oluşturup:

  - *rules* altına aşağıdaki kuralı ekledik. Bu sayede sadece *release-* prefix'i ile başlayan bir tag oluşturduğumuzda çalışacak.

    - **Gitlab CI, düzenli ifadeleri (regex) destekler.**

```yaml
...
  rules:
    - if: $CI_COMMIT_TAG =~ /^release-\d+\.\d+$/
...
```

  - *release* altında ise Gitlab'da projemizin release'inde içermesini istediğimiz bilgileri ve paketleri ekledik.

  - *release.description* altında daha detaylı bilgiler yazabilirsiniz.

  - *release.assets* altında da ikilik dosyalarımızın Package Registry URL'lerini girdik. Uzun uzun yazmamak için *PACKAGE_PATH* değişkenini oluşturup hem Build/curl komutunda hem de Release altındaki asset'lerde bunu kullandım.

&nbsp;

<div align="center">
    <img src=".img/13.png">
</div>

&nbsp;

Pipeline başarı ile tamamlanırsa projemizde *Deploy/Release* altında, oluşturmuş olduğumuz *release* 'i görebilirsiniz.

Packages alanı altındaki dosyaları buradan indirebiliriz.

&nbsp;

<div align="center">
    <img src=".img/14.png">
</div>

&nbsp;

Gitlab Package Registry normalde yetkisiz kullanıcıların erişimine kapalıdır. Eğer projenizi paylaşmak isterseniz bir kaç ayarlama yapmak gerekecek.

Projemizde *Settings/General* altına gelip *Visibility, project features, permissions* alanını genişletelim.

*Project visibility* 'yi *Public* olarak değiştirelim.

&nbsp;

<div align="center">
    <img src=".img/15.png">
</div>

&nbsp;

Eğer *Package registry* açık değilse (Projemizi public yaptığımızda açık olmalı) bunu da açalım.

Diğerlerinden istediğinizi kapatabilirsiniz.

&nbsp;

<div align="center">
    <img src=".img/16.png">
</div>

&nbsp;

Son olarak değişikliklerimizi kaydedelim.

&nbsp;

<div align="center">
    <img src=".img/17.png">
</div>

&nbsp;

Bu sayede muhteşem projelerinizi özgür lisanslar ile paylaşabilirsiniz :)
