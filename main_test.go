package main

import (
	"reflect"
	"testing"
)

func TestListIPs(t *testing.T) {
	tests := []struct {
		name     string
		subnets  []string
		expected []string
	}{
		{
			name:    "Single subnet",
			subnets: []string{"192.168.1.0/30"},
			expected: []string{
				"192.168.1.0",
				"192.168.1.1",
				"192.168.1.2",
				"192.168.1.3",
			},
		},
		{
			name:    "Multiple subnets",
			subnets: []string{"192.168.1.0/30", "10.0.0.0/30"},
			expected: []string{
				"192.168.1.0",
				"192.168.1.1",
				"192.168.1.2",
				"192.168.1.3",
				"10.0.0.0",
				"10.0.0.1",
				"10.0.0.2",
				"10.0.0.3",
			},
		},
		{
			name:    "Invalid subnet",
			subnets: []string{"invalid_subnet"},
			expected: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual, err := listIPs(tt.subnets)
			if tt.expected == nil && err == nil {
				t.Errorf("expected error but got nil")
			}
			if tt.expected != nil && err != nil {
				t.Errorf("did not expect error but got %v", err)
			}
			if !reflect.DeepEqual(actual, tt.expected) {
				t.Errorf("expected %v, got %v", tt.expected, actual)
			}
		})
	}
}
